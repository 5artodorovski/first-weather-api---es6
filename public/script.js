  
window.addEventListener('load', ()=> {
  let long;
  let lat;
  let temperatureDescription = document.querySelector('.temperature-description');
  let temepratureDegree = document.querySelector('.temperature-degree');
  let locationTimezone = document.querySelector('.location-timezone');
  let temperatureSection = document.querySelector('.temperature');
  let windDSpeed = document.querySelector('.wind-speed');
  let temperatureSpan = document.querySelector('.temperature span');

  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
      long = position.coords.longitude;
      lat = position.coords.latitude;
      // const proxy = `https://cors-anywhere.herokuapp.com:`;
      let api = `https://cors-anywhere.herokuapp.com:/https://api.darksky.net/forecast/d14522bd6d449648436bea625fc2c6c9/${lat},${long}`;

      fetch(api)
      .then(data => {
        return data.json();
      })
      .then (data => {
        let {temperature, summary, icon, windSpeed} = data.currently;
        temepratureDegree.textContent = temperature;
        temperatureDescription.textContent = summary;
        locationTimezone.textContent = data.timezone;
        windDSpeed.textContent = windSpeed;

        // Formula for celsius
        let celsius = (temperature - 32) * (5/9);

        // Set icon
        setIcons(icon, document.querySelector('.icon'));

        // Change temeperature format on click
        temperatureSection.addEventListener('click', () => {
          if(temperatureSpan.textContent === "F") {
            temperatureSpan.textContent = "C";
            temepratureDegree.textContent = Math.floor(celsius);
          }else {
            temperatureSpan.textContent = "F";
            temepratureDegree.textContent = temperature;
          }
        })
      })
    });
  }
  function setIcons(icon, iconID) {
    skycons = new Skycons({"color": "white"});
    const currentIcon = icon.replace(/-/g, "_").toUpperCase();
    skycons.play();
    return skycons.set(iconID, Skycons[currentIcon]);
  }
})